#! /user/var/env python3
"""This file contains functions to perform josephus problem simulations on
Armies created using the class definitions.  If called directly, it will
perform simulation with n = 128 and k=1, strip formatting tags and print
results to stdout."""

from army import Army


def run_simulations(skip_number, number_simulations):
    for number_soldiers in range(number_simulations):
        result = run_round(number_soldiers, skip_number + 1)
        yield result


def run_round(n, k):
    # Creates army of n soldiers
    soldiers = Army(n + 1)
    soldiers.create_soldiers()
    # Runs the simulation
    survivor = soldiers.kill_soldiers(k)
    # Prints results.
    return f'When <i>n</i> is {n+1} and <i>k</i> is {k-1}, soldier ' \
           f'{survivor.number} survives!'

if __name__ == '__main__':
    # If this file is run directly, performs simulation with n = 128 and
    # k=1, strips formatting tags and prints results to stdout.
    for simulation in (run_simulations(skip_number=1, number_simulations=128)):
        simulation = simulation.replace("<i>", "", 2)
        simulation = simulation.replace("</i>", "", 2)
        print(simulation)
