#! /user/var/env python3
"""
This file contains class definitions and modules useful in solving
the generic case of the Josephus problem through simulation.  It implements
the following classes:
    Soldier: creates a soldier for use in simulation.
    Army: creates a list object to contain Soldiers.
"""


class Soldier:
    """
    Represents a soldier.
    
    Keyword arguments:
    number: serial number in order of creation by Army.create_soldiers (int)
    """
    def __init__(self, number):
        self.number = number
        self.alive = True

    def __str__(self):
        """Returns a printable string in place of an object pointer."""
        return f'{self.number}: alive={self.alive}'

    def kill(self):
        """Sets the soldier's status to represent his death"""
        self.alive = False


class Army:
    """Represents an army of soldiers."""
    def __init__(self, n):
        self.n = n
        self.soldiers = []

    def create_soldiers(self):
        """Adds n soldiers to Army."""
        for number in range(1, self.n + 1):
            self.soldiers.append(Soldier(number))

    def kill_soldiers(self, k):
        """
        Kills every kth soldiers in Army, starting with k until only one
        soldier remains.
        """
        # Initialize counters for loop control.
        soldier_count = self.n
        step_count = k
        # Kills until one soldier remains.
        while soldier_count > 1:
            for soldier in self.soldiers:
                # Skips k-1 soldiers, decrements the counter.
                if step_count > 0 and soldier.alive is True:
                        step_count -= 1
                # Kills the kth soldier and resets the counter.
                if step_count == 0:
                    soldier.kill()
                    step_count = k
                    soldier_count -= 1
        # Find and return the surviving Soldier.
        for soldier in self.soldiers:
            if soldier.alive is True:
                return soldier


def main(n, k):
    # Creates army of n soldiers
    soldiers = Army(n + 1)
    soldiers.create_soldiers()
    # Runs the simulation
    survivor = soldiers.kill_soldiers(k)
    # Prints results.
    print(f'When n is {n}, soldier {survivor.number} survives!')

if __name__ == '__main__':
        print(__doc__)
