#! /user/var/env python3.6
"""
Primary application file for GUI Josephus problem simulator.
"""

import sys
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from simulator import run_simulations


class MainWindow(QMainWindow):
    """Creates the main window and populates it with widgets."""
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Josephus Simulator')
        self.resize(360, 480)

        # Create main_menu
        self.main_menu = QMenuBar()

        # Create File menu in Main Menu.  This will appear in the main
        # toolbar on OSX.
        self.file_menu = self.main_menu.addMenu('File')

        # Create save item in File menu
        self.save_item = QAction('Save')
        self.save_item.triggered.connect(self.save_stuff)
        self.save_item.setShortcut("Ctrl+S")
        self.file_menu.addAction(self.save_item)

        # Create help menu in Main Menu.
        self.help_menu = self.main_menu.addMenu('Help')
        self.help_menu = self.main_menu.addMenu('Help')

        # Create About item in Help menu and link to function creating dialog
        # box. This will appear in the application menu on OSX.
        self.about = QAction('About')
        self.about.triggered.connect(self.about_dialogue)
        self.help_menu.addAction(self.about)

        # Create Instructions item in Help menu and link to function creating
        # dialog box.
        self.info = QAction('Instructions')
        self.info.triggered.connect(self.info_dialogue)
        self.help_menu.addAction(self.info)

        # create central layout
        self.central_layout = QVBoxLayout()

        # create central widget
        self.central_widget = QWidget()
        self.central_widget.setLayout(self.central_layout)
        self.setCentralWidget(self.central_widget)

        # create settings widgets
        self.number_simulations = QSpinBox()
        self.number_simulations.setRange(1, 1_024)
        self.number_skips = QSpinBox()
        self.number_skips.setRange(1, 100)
        self.run_button = QPushButton('Run Simulation')
        self.run_button.clicked.connect(self.run_button_clicked)

        # create settings group box
        self.settings_group_box = QGroupBox("Simulation Settings")

        # Create layout for settings group box
        self.settings_form = QFormLayout(self.settings_group_box)

        # Add settings widgets to settings group box
        self.settings_form.addRow(QLabel("Number of simulations <i>(n)</i>: "),
                                  self.number_simulations)
        self.settings_form.addRow(QLabel("Number to skip <i>(k)</i>: "),
                                  self.number_skips)
        self.settings_form.addRow(self.run_button)

        # create output box
        self.output_box = QTextEdit()

        # add settings group box and output box to central layout
        self.central_layout.addWidget(self.settings_group_box,
                                      alignment=Qt.AlignHCenter)
        self.central_layout.addWidget(self.output_box)

    # Define methods for use in QMainWindow.

    def run_button_clicked(self):
        """
        Method to run simulation when run button is pushed.  Clears the
        output text box and runs josephus.run_simulations, appending the
        output to the output box.  Note that run_simulations uses yeild
        method to output results from multiple rounds of a for loop
        """
        self.output_box.clear()
        for result in run_simulations(self.number_skips.value(),
                                      self.number_simulations.value()):
            self.output_box.append(result)

    def save_stuff(self):
        """
        Method to save results from output box to plain text file. Opens a
        QFileDialog to allow selection or creation of output file.
        """
        save_file = "".join(QFileDialog.getSaveFileName(filter=".txt"))
        if save_file is '':
            return
        with open(str(save_file), 'w') as f:
                f.write(self.output_box.toPlainText())

    def info_dialogue(self):
        """
        Method to open an information message box containing instructions
        for using the simulator.
        """
        info_text = "This simulator tests the general case of the Josephus " \
                    "problem, where members of a group of size <i>n</i> are" \
                    "eliminated serially, skipping <i>k</i> members between " \
                    "each elimination."
        info_box = QMessageBox()
        info_box.setIcon(QMessageBox.Information)
        info_box.setText("Instructions")
        info_box.setInformativeText(info_text)
        info_box.setWindowTitle("Window Title")
        info_box.setDetailedText("Instructions go here.")
        info_box.setStandardButtons(QMessageBox.Ok)
        info_box.setEscapeButton(QMessageBox.Close)
        info_box.exec_()

    def about_dialogue(self):
        """
        Method to open an about message box containing information about
        the program.
        """
        about_text = "Josephus Simulator by Richard Spaulding, Jr. is " \
                     "written in Python and requires Python 3.6.0 or " \
                     "later. This project uses the GPL version of PyQt5 and" \
                     "the LGPL version of Qt. You may find license information" \
                     " and source code at " \
                     "https://gitlab.com/thelatinist/josephus."
        about_box = QMessageBox()
        about_box.setIcon(QMessageBox.Information)
        about_box.setText("About")
        about_box.setInformativeText(about_text)
        about_box.setWindowTitle("About Josephus Simulator")
        about_box.setDetailedText("")
        about_box.setStandardButtons(QMessageBox.Ok)
        about_box.setEscapeButton(QMessageBox.Close)
        about_box.exec_()


if __name__ == '__main__':
    # Creates application windows and widget, shows it, and exits to event loop.
    application = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(application.exec_())
